package constants

const FlowStatusDraft = "draft"
const FlowStatusPublish = "publish"
const ActionTypePostBack = "post_back"
const ActionTypeSendSuccess = "action_send_success"
const ActionTypeSendFailed = "action_send_failed"
const ActionTypeDefault = "action_default"
const ActionTypeUserReceivedMessage = "action_user_received_message"
const ActionTypeUserViewedMessage = "action_user_viewed_message"
