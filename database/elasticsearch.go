package database

import (
	"context"
	"fmt"
	"gopkg.in/olivere/elastic.v5"
	"os"
)

var ElasticSearch *elastic.Client

func InitElasticSearch() {
	ElasticSearch = connectEL()
}

func connectEL() *elastic.Client {
	fmt.Printf("\n start connect elasticsearch")
	client, err := elastic.NewClient(elastic.SetURL("http://157.245.58.108:9404"), elastic.SetSniff(false))
	if err != nil {
		fmt.Printf("\n Init elasticsearch Err: %v", err)
		panic(err)
	}
	info, code, err := client.Ping(os.Getenv("ELASTIC_SEARCH_URI")).Do(context.Background())
	if err != nil {
		// Handle error
		panic(err)
	}
	fmt.Printf("Elasticsearch returned with code %d and version %s\n", code, info.Version.Number)
	version, err := client.ElasticsearchVersion(os.Getenv("ELASTIC_SEARCH_URI"))
	if err != nil {
		fmt.Printf("\n Init elasticsearch Err: %v", err)
	} else {
		fmt.Printf("\n elasticsearch Ver: %v", version)
	}
	return client
}
