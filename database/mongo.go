package database

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
)

var mongoDatabase *mongo.Database

func InitMongo() {
	mongoDatabase = connect()
}
func connect() *mongo.Database {
	fmt.Printf("start connect mongo")
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(os.Getenv("MONGO_SHOPPRO_CONNECTION_STRING")))
	if err != nil {
		fmt.Printf("Init Mongo Err: %v", err)
		panic(err)
	}
	fmt.Printf("Mongo success")
	return client.Database(os.Getenv("MONGO_SHOPPRO_DB"))
}

func ZaloCollection() *mongo.Collection{
	return mongoDatabase.Collection("zalo_message")
}
func ConversationCollection() *mongo.Collection {
	collection := mongoDatabase.Collection("conversation")
	return collection
}

func WalletCollection() *mongo.Collection {
	collection := mongoDatabase.Collection("wallet")
	return collection
}

func WalletTransactionCollection() *mongo.Collection {
	collection := mongoDatabase.Collection("wallet_transaction")
	return collection
}

func FBMessageCollection() *mongo.Collection {
	collection := mongoDatabase.Collection("fb_message")
	return collection
}

func HotelBookingInfo() *mongo.Collection {
	collection := mongoDatabase.Collection("hotel_booking_info")
	return collection
}

func ScheduledMessage() *mongo.Collection {
	collection := mongoDatabase.Collection("scheduled_message")
	return collection
}

func NotificationCollection() *mongo.Collection {
	collection := mongoDatabase.Collection("notification")
	return collection
}

func CustomerActivityCollection() *mongo.Collection {
	collection := mongoDatabase.Collection("customer_activity")
	return collection
}
func VoucherCollection() *mongo.Collection {
	collection := mongoDatabase.Collection("voucher")
	return collection
}
func VoucherTimeCollection() *mongo.Collection {
	collection := mongoDatabase.Collection("voucher_time")
	return collection
}
func VoucherConfigCollection() *mongo.Collection {
	collection := mongoDatabase.Collection("voucher_config")
	return collection
}
func MemberRankCollection() *mongo.Collection {
	collection := mongoDatabase.Collection("member_rank")
	return collection
}
func MemberConfigRankCollection() *mongo.Collection {
	collection := mongoDatabase.Collection("member_config_rank")
	return collection
}
func CustomerUseVoucher() *mongo.Collection {
	collection := mongoDatabase.Collection("customer_use_voucher")
	return collection
}