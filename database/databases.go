package database

import (
	"fmt"
	"github.com/getsentry/sentry-go"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"runtime/debug"
	"time"
)

var DB *gorm.DB

func InitMysql() {
	DB = MysqlConnect()
}

func HandleLog() logger.Writer {
	return log.New(os.Stdout, "\r\n", log.LstdFlags)
}

func MysqlConnect() *gorm.DB {
	newLogger := logger.New(
		HandleLog(), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logger.Info, // Log level
			IgnoreRecordNotFoundError: true,        // Ignore ErrRecordNotFound error for logger
			Colorful:                  false,       // Disable color
		},
	)
	fmt.Println("start connect mysql")
	db, err := gorm.Open(mysql.Open(os.Getenv("MYSQL_CONNECTION_STRING")), &gorm.Config{Logger: newLogger})
	if err != nil {
		fmt.Printf("err %v", err)
		err = fmt.Errorf("%v, %s", err, string(debug.Stack()))
		sentry.CurrentHub().Recover(err)
		sentry.Flush(time.Second * 5)
		panic(err)
		return nil
	}
	fmt.Println("mysql connected")
	return db
}
