package elastic_search

import (
	"common/database"
	"context"
	"gopkg.in/olivere/elastic.v5"
	"log"
	"time"
)

const IndexFlowTransaction = "flow_transaction"
const TypeZNSTransaction = "zns_transaction"
const IndexZNSPartnerTransaction = "zns_partner_transaction"
const TypeZNSPartnerTima = "tima"
const TypeZNSPartnerTimaHub = "tima_hub"

type FlowTransaction struct {
	PageId        string `json:"page_id" bson:"page_id"`
	ShopId        int    `json:"shop_id" bson:"shop_id"`
	FlowId        int    `json:"flow_id" bson:"flow_id"`
	FlowName      string `json:"flow_name" bson:"flow_name"`
	FlowContextId string `json:"flow_context_id" bson:"flow_context_id"`
	IsSuccess     bool   `json:"is_success" bson:"is_success"`
	TriggerId     int    `json:"trigger_id" bson:"trigger_id"`
	TriggerName   string `json:"trigger_name" bson:"trigger_name"`
	TemplateId    int    `json:"template_id" bson:"template_id"`
	TemplateName  string `json:"template_name" bson:"template_name"`
	CustomerId    int    `json:"customer_id" bson:"customer_id"`
	Amount        int    `json:"amount" bson:"amount"`
	PhoneNumber   string `json:"phone_number" bson:"phone_number"`
	CustomerName  string `json:"customer_name" bson:"customer_name"`
	Result        string `json:"result" bson:"result"`
	CreatedAt     int64  `json:"created_at" bson:"created_at"`
}

type ZNSTransactionQuery struct {
	Path   string `json:"path" schema:"path"`
	TriggerId     int       `json:"trigger_id" schema:"trigger_id"`
	FlowId        int       `json:"flow_id" schema:"flow_id"`
	IsSuccess int    `json:"is_success" schema:"is_success"`
	PageId        string    `json:"page_id" schema:"page_id"`
	Page          int       `json:"page" schema:"page"`
	ShopId        int       `json:"shop_id" schema:"shop_id"`
	PageSize      int       `json:"page_size" schema:"page_size"`
	From          time.Time `json:"from" schema:"from"`
	To            time.Time `json:"to" schema:"to"`
}

type FlowTransactionExport struct {
	Amount int `json:"amount"`
	CreatedAt time.Time `json:"created_at"`
	CustomerId int  `json:"customer_id"`
	CustomerName string `json:"customer_name"`
	FlowContextId string `json:"flow_context_id"`
	FlowId         int `json:"flow_id"`
	FlowName        string `json:"flow_name"`
	IsSuccess       bool   `json:"is_success"`
	PageId    string `json:"page_id"`
	PhoneNumber string `json:"phone_number"`
	Result  string `json:"result"`
	ShopId  int `json:"shop_id"`
	TemplateId int `json:"template_id"`
	TemplateName string `json:"template_name"`
	TriggerId    int `json:"trigger_id"`
	TriggerName  string `json:"trigger_name"`
}
func InsertFlowTransaction(transaction FlowTransaction) {
	put1, err := database.ElasticSearch.Index().
		Index(IndexFlowTransaction).
		Type(TypeZNSTransaction).
		BodyJson(transaction).
		Do(context.Background())
	if err != nil {
		// Handle error
		log.Printf("error elastic: %v", err)
	} else {
		log.Printf("Indexed tweet %s to index %s, type %s\n", put1.Id, put1.Index, put1.Type)
	}
}

func GetFlowTransactions(znsParams ZNSTransactionQuery) *elastic.SearchResult {
	var queries []elastic.Query
	queries = append(queries, elastic.NewTermQuery("shop_id", znsParams.ShopId))
	if znsParams.PageId != "" {
		queries = append(queries, elastic.NewTermQuery("page_id", znsParams.PageId))
	}
	if znsParams.FlowId != 0 {
		queries = append(queries, elastic.NewTermQuery("flow_id", znsParams.FlowId))
	}
	if znsParams.IsSuccess==1 {
		queries = append(queries, elastic.NewTermQuery("is_success", true))
	}else if znsParams.IsSuccess==-1 {
		queries = append(queries, elastic.NewTermQuery("is_success", false))
	}
	if znsParams.TriggerId != 0 {
		queries = append(queries, elastic.NewTermQuery("trigger_id", znsParams.TriggerId))
	}
	if !znsParams.From.IsZero() && !znsParams.To.IsZero() {
		queries = append(queries, elastic.NewRangeQuery("created_at").Gte(znsParams.From.Unix()).Lte(znsParams.To.Unix()))
	}
	query := elastic.NewBoolQuery().
		Must(queries...)
	result, err := database.ElasticSearch.Search().Index(IndexFlowTransaction).Type(TypeZNSTransaction).Query(query).
		Aggregation("total_amount", elastic.NewSumAggregation().Field("amount")).Sort("created_at",false).
		From((znsParams.Page - 1) * znsParams.PageSize).Size(znsParams.PageSize).
		Do(context.Background())
	if err != nil {
		// Handle error
		log.Printf("error elastic: %v", err)
	}
	return result
}
