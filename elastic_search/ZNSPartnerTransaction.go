package elastic_search

import (
	"common/database"
	"context"
	"gopkg.in/olivere/elastic.v5"
	"log"
	"time"
)

type ZNSPartnerTransaction struct {
	ShopId       int         `json:"shop_id"`
	PageId       string      `json:"page_id"`
	Cost         int         `json:"cost"`
	Amount       int         `json:"amount"`
	Creator      int         `json:"creator"`
	Partner      string      `json:"partner"`
	TemplateId   int         `json:"template_id"`
	TemplateName string      `json:"template_name"`
	PhoneNumber  string      `json:"phone_number"`
	CustomerName string      `json:"customer_name"`
	CustomerId   int         `json:"customer_id"`
	Message      interface{} `json:"message"`
	Reason       interface{} `json:"reason"`
	IsSuccess    bool        `json:"is_success"`
	CreatedAt    int64       `json:"created_at" bson:"created_at"`
}

func InsertZNSPartnerTransaction(transaction ZNSPartnerTransaction, typeE string) string {
	put1, err := database.ElasticSearch.Index().
		Index(IndexZNSPartnerTransaction).
		Type(typeE).
		BodyJson(transaction).
		Do(context.Background())
	if err != nil {
		// Handle error
		log.Printf("error elastic: %v", err)
	}
	log.Printf("Indexed tweet %s to index %s, type %s\n", put1.Id, put1.Index, put1.Type)
	return put1.Id
}

func GetZNSTimaPartnerTransactions(from time.Time, to time.Time, elasticType string, page int, pageSize int) *elastic.SearchResult {
	var queries []elastic.Query
	if !from.IsZero() && !to.IsZero() {
		f := from.Unix()
		t := to.Unix()
		queries = append(queries, elastic.NewRangeQuery("created_at").Gte(f).Lte(t))
	}
	query := elastic.NewBoolQuery().
		Must(queries...)
	result, err := database.ElasticSearch.Search().Index(IndexZNSPartnerTransaction).Type(elasticType).Query(query).
		Aggregation("total_amount", elastic.NewSumAggregation().Field("amount")).
		From((page-1)*pageSize).Size(pageSize).Sort("created_at", false).
		Do(context.Background())
	if err != nil {
		// Handle error
		log.Printf("error elastic: %v", err)
	}
	return result
}
